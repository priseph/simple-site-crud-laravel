<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //Defining and attributing a comment with a user's Post
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Function responsible for comment replies
    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }
}
