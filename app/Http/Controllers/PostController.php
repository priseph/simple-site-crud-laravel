<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Session;

class PostController extends Controller
{
    
    public function __construct()
    {
        return $this->middleware('auth');
    }


    //Responsible for the post listing 
    public function index()
	{
	    $posts = Post::all();

	    return view('index', compact('posts'));
	}

	//Responsible for the post creation form
    public function create()
    {
        return view('post');
    }

    //Responsible for the post storage
    public function store(Request $request)
    {

    	$this->validate($request, [
			'postImage' => 'image|mimes:jpeg,png,jpg,gif,mov,mpeg4,avi,svg|max:2048',
            'title' => 'required',
			'body' => 'required'
            
        ]);

        // store code
        $post =  new Post;
        $post->title = $request->get('title');
        $post->body = $request->get('body');

        if($request->hasfile('postImage'))
         {
            $file = $request->file('postImage');
            $name=time().$file->getClientOriginalName();
            $file->move(public_path().'/postImage/', $name);
			$post->postImage=$name;
         }

        $post->save();

        return redirect('posts');
    }

    //Responsible for Showing a single post
    public function show($id)
	{
	    $post = Post::find($id);

	    return view('show', compact('post'));
	}



    //we get post for edit
    public function editPost($id)
    {

        $post = Post::find($id);
        return view('editPost', [ 'post' => $post ]);

    }



    public function update(Request $request, $id)
    {

        $this->validate($request, [
			'postImage' => 'image|mimes:jpeg,png,jpg,gif,mov,mpeg4,avi,svg|max:2048',
            'title' => 'required',
			'body' => 'required'
            
        ]);

        $post = Post::find($id);
		$post->title = $request->title;
        $post->body = $request->body;

		if($request->hasfile('postImage'))
         {
            $file = $request->file('postImage');
            $name=time().$file->getClientOriginalName();
            $file->move(public_path().'/postImage/', $name);
			$post->postImage = $name;
         }
        

		
        $post->save();
        Session::flash('success', 'Successfully Updated Post.');
		return redirect()->back();

    }

	//Responsible for Deleting a single post
	public function destroy($id)
    {
        //
        $post = Post::find( $id );

        $post->delete();
        Session::flash('success', 'Post Deleted');

        return redirect()->back();

    }
}
