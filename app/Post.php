<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	//attributing a post to a User
	public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Polymorphic relationships between the Post and Comment models
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }
}
