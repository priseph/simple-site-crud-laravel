<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route to view post
Route::get('/post/create', 'PostController@create')->name('post.create');
//Route to store post
Route::post('/post/store', 'PostController@store')->name('post.store');
//Route to update post
Route::post('/post/update/{id}', 'PostController@update')->name('post.update');
//Route to rediret to saved Post
Route::get('/posts', 'PostController@index')->name('posts');
//Route to Show a single Post
Route::get('/post/show/{id}', 'PostController@show')->name('post.show');
//Route to delete a single Post
Route::get('/post/destroy/{id}', 'PostController@destroy')->name('post.destroy');
//Route to Edit a single Post
Route::get('/post/editPost/{id}', 'PostController@editPost')->name('post.editPost');

//Route to store comment
Route::post('/comment/store', 'CommentController@store')->name('comment.add');
//Route to store replies
Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');