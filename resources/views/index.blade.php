@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if(Session::has('success'))
            <div class="alert alert-success text-center" role="alert"><h4 class="text-center"><strong>{{ Session::get('success') }}</strong></h4></div>
        @endif
            <table class="table table-striped">
                <thead>
                    <th>Title</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{{ $post->title }}</td>
                    <td>
                        <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary">View Post</a>
                    &nbsp;
                    <a href="{{ route('post.editPost', $post->id) }}" class="btn btn-primary">Edit Post</a>
                    &nbsp;
                        <a href="{{ route('post.destroy', $post->id) }}" class="btn btn-primary">Delete Post</a>
                    </td>
                </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
@endsection