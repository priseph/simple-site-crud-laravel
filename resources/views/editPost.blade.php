@extends('layouts.app')
<style>
    .display-comment .display-comment {
        margin-left: 40px
    }
</style>
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <a href="{{ route('posts') }}" style="margin-left: 10px;">Back to Posts</a>
                <div class="card-body">
                    <div class="col-md-12">
                    @if(Session::has('success'))
                        <div class="alert alert-success text-center" role="alert"><h4 class="text-center"><strong>{{ Session::get('success') }}</strong></h4></div>
                    @endif
                    <p><b><img src=" {{url('/').'/postImage/'}}{{ $post->postImage }}"  alt="{{ $post->title }}" style=" height:300px"/></b></p>
                    
                    <form method="post" action="{{ route('post.update', $post->id) }}" enctype="multipart/form-data">
                        <div class="form-group">
                            @csrf
                            <label class="label">Post Title: </label>
                            <input type="text" name="title" class="form-control" value="{{ $post->title }}" required/>
                        </div>
                        <div class="form-group">
                            <label class="label">Post Body: </label>
                            <textarea name="body" rows="10" cols="30" class="form-control" required>{{ $post->body }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Upload Image file if any (max file size: 1MB)</label>
                            <input type="file" name="postImage" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" />
                        </div>
                         <a href="{{ route('posts') }}" style="margin-left: 10px;">Back to Posts</a>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection